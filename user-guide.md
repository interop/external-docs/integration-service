# Integration Service User Guide

Refer to this documentation for general usage of the Integration Service.

For documentation to help with migrating to the Integration Service from the old ESB, see [this guide](migration-user-guide.md).

[[_TOC_]]

## Hostnames

Test: test.integration.services.wisc.edu

Production: integration.services.wisc.edu

## Endpoints

All endpoints are protected by IP Address.

There are 3 CIDR blocks for UW-Madison Campus IP address ranges.
More information on UW IP address ranges can be found [here](https://kb.wisc.edu/ns/page.php?id=3988).

128.104.0.0/16 and 144.92.0.0/16 are CIDR blocks for servers assigned to the UW-Madison Campus.
146.151.128.0/17 is the CIDR block assigned to WiscVPN Static IP addresses as well as other servers.

If the IP address is not covered by the 3 CIDR blocks, then add your IP addresses to be on the allow-list, either by submitting a merge request to [edit this file](https://git.doit.wisc.edu/interop/integration-service/proxy/-/blob/master/terraform/variables.tf), or by contacting the API Team: api@doit.wisc.edu

### WSDL

Endpoint: https://{hostname}/wsdl/{service}.wsdl

Responses:

- `200`
    - Successful WSDL response.
- `404`
    - WSDL file was not found.
    
### SOAP

Endpoint: https://{hostname}/soap/{service}

All SOAP services use ws-security. Example:

```xml
   <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <UsernameToken>
      <Username>username</Username>
      <Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">secretsauce</Password>
    </UsernameToken>
  </Security>
```

Responses:

- Successful Response
    - Response code depends on the backend SOAP service.
- `500`
    - There was a problem authenticating or authorizing the SOAP request.
    
### REST

Endpoint: https://{hostname}/rest/{service}

All REST services use [basic auth](https://tools.ietf.org/html/rfc7617).

Responses:

- Successful Response
    - Response code depends on the backend REST service.
- `400`
    - Bad request.
- `401`
    - Invalid username or password.
- `403`
    - Unauthorized to access the requested service.
- `404`
    - Service not found.
- `502`
    - Backend service timed out.

### Health

Endpoint: https://{hostname}/health

Method: `GET`

Lightweight health endpoint for confirming availability of the Integration Service.

Responses:

- `200`
    - Integration Service is available.
- `500` - `599`
    - Integration Service is down.

## Timeouts

REST and SOAP services have a default timeout of 50s. If a backend service fails to respond within the allowed time, the Integration Service will return an error. 

Note that some backends might have a non-default timeout configured. Contact us if you need to know the specific timeout for an API.  If you're a service provider, let us know if you need to change the timeout for your service.

## Information for Service Providers

Here are some general expectations for service providers, and what can be expected of the Integration Service.

- Requests proxied to backend services will come from these IP addresses: Test: `3.230.240.5` Prod: `3.19.12.147`
- Service providers should make sure they have a mechanism in place to alert them if their service is down. The Integration Service will not alert service providers if their service is down.
- The DoIT API Team will be alerted if the Integration Service is unavailable, but it will not monitor each individual backend service.
- SOAP request payloads will be forwarded with the ws-security username, but the ws-security password will be removed to match the old ESB mediator behavior.

## Get Help

Contact the DoIT API Team to get help and support: api@doit.wisc.edu
