# Migration User Guide

This guide is meant to help ESB service consumers and service providers migrate to the new Integration Service.

For more general usage documentation, see [this file](./user-guide.md).

[[_TOC_]]

## REST Services

|Service|New Endpoint (test)| New Endpoint (prod)|
|-------|-------------------|--------------------|
|AdviseeServices|https://test.integration.services.wisc.edu/rest/AdviseeServices|https://integration.services.wisc.edu/rest/AdviseeServices|
|dnumber|https://test.integration.services.wisc.edu/rest/dnumber|https://integration.services.wisc.edu/rest/dnumber|
|extid|https://test.integration.services.wisc.edu/rest/extid|https://integration.services.wisc.edu/rest/extid|
|enrollment-services|https://test.integration.services.wisc.edu/rest/enrollment-services|https://integration.services.wisc.edu/rest/enrollment-services|
|L3_CAOS_REST|https://test.integration.services.wisc.edu/rest/L3_CAOS_REST|https://integration.services.wisc.edu/rest/L3_CAOS_REST|
|SoarStudentServices|https://test.integration.services.wisc.edu/rest/SoarStudentServices|https://integration.services.wisc.edu/rest/SoarStudentServices|
|TranscriptServices|https://test.integration.services.wisc.edu/rest/TranscriptServices|https://integration.services.wisc.edu/rest/TranscriptServices|

## SOAP Services

### Test

|Service|Old WSDL (test)|New WSDL (test)|
|-------|---------------|---------------|
|CAOS_1.0|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.0/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.0.wsdl|
|CAOS_1.1|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.1/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.1.wsdl|
|CAOS_1.2|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.2/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.2.wsdl|
|CAOS_1.3|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.3/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.3.wsdl|
|CAOS_1.4|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.4/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.4.wsdl|
|CAOS_1.5|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.5/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.5.wsdl|
|CAOS_1.6|http://esbtest.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.6/chub.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.6.wsdl|
|CAOS_1.7|http://esbtest.services.wisc.edu/esbv2/CAOS/WebService/caos-ws-1.7/caos.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.7.wsdl|
|CAOS_1.8|http://esbtest.services.wisc.edu/esbv2/CAOS/WebService/caos-ws-1.8/caos.wsdl|https://test.integration.services.wisc.edu/wsdl/CAOS_1.8.wsdl|
|CRIS_1.2|http://esbtest.services.wisc.edu/esbv2/CRIS/WebService/courseRoster.wsdl|https://test.integration.services.wisc.edu/wsdl/CRIS_1.2.wsdl|
|L3|http://esbtest.services.wisc.edu/esbv2/CAOS/WebService/l3-ws-1.0/l3.wsdl|https://test.integration.services.wisc.edu/wsdl/L3.wsdl|
|IMS|http://esbtest.services.wisc.edu/esbv2/UWDS/WebService/IMS.wsdl|https://test.integration.services.wisc.edu/wsdl/IMS.wsdl|
|photoidservice|http://esbtest.services.wisc.edu/esbv2/UWDS/WebService/photoidservice.wsdl|https://test.integration.services.wisc.edu/wsdl/photoidservice.wsdl|
|UDS|http://esbtest.services.wisc.edu/esbv2/UWDS/WebService/UDS.wsdl|https://test.integration.services.wisc.edu/wsdl/UDS.wsdl|
|UDSPerson|http://esbtest.services.wisc.edu/esbv2/UWDS/WebService/UDSPerson.wsdl|https://test.integration.services.wisc.edu/wsdl/UDSPerson.wsdl|
|UDSPerson2|http://esbtest.services.wisc.edu/esbv2/UWDS/WebService/UDSPerson2.wsdl|https://test.integration.services.wisc.edu/wsdl/UDSPerson2.wsdl|
|ResourceBridge|http://esbtest.services.wisc.edu/esbv2/ResourceBridge/WebService/ResourceBridge.wsdl|https://test.integration.services.wisc.edu/wsdl/ResourceBridge.wsdl|
|ServiceAccount|http://esbtest.services.wisc.edu/esbv2/UWDS/WebService/ServiceAccount.wsdl|https://test.integration.services.wisc.edu/wsdl/ServiceAccount.wsdl|

### Production

|Service|Old WSDL (prod)|New WSDL(prod)|
|-------|---------------|--------------|
|CAOS_1.0|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.0/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.0.wsdl|
|CAOS_1.1|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.1/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.1.wsdl|
|CAOS_1.2|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.2/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.2.wsdl|
|CAOS_1.3|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.3/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.3.wsdl|
|CAOS_1.4|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.4/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.4.wsdl|
|CAOS_1.5|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.5/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.5.wsdl|
|CAOS_1.6|http://esb.services.wisc.edu/esbv2/CHUB/WebService/chub-ws-1.6/chub.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.6.wsdl|
|CAOS_1.7|http://esb.services.wisc.edu/esbv2/CAOS/WebService/caos-ws-1.7/caos.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.7.wsdl|
|CAOS_1.8|http://esb.services.wisc.edu/esbv2/CAOS/WebService/caos-ws-1.8/caos.wsdl|https://integration.services.wisc.edu/wsdl/CAOS_1.8.wsdl|
|CRIS_1.2|http://esb.services.wisc.edu/esbv2/CRIS/WebService/courseRoster.wsdl|https://integration.services.wisc.edu/wsdl/CRIS_1.2.wsdl|
|L3|http://esb.services.wisc.edu/esbv2/CAOS/WebService/l3-ws-1.0/l3.wsdl|https://integration.services.wisc.edu/wsdl/L3.wsdl|
|IMS|http://esb.services.wisc.edu/esbv2/UWDS/WebService/IMS.wsdl|https://integration.services.wisc.edu/wsdl/IMS.wsdl|
|photoidservice|http://esb.services.wisc.edu/esbv2/UWDS/WebService/photoidservice.wsdl|https://integration.services.wisc.edu/wsdl/photoidservice.wsdl|
|UDS|http://esb.services.wisc.edu/esbv2/UWDS/WebService/UDS.wsdl|https://integration.services.wisc.edu/wsdl/UDS.wsdl|
|UDSPerson|http://esb.services.wisc.edu/esbv2/UWDS/WebService/UDSPerson.wsdl|https://integration.services.wisc.edu/wsdl/UDSPerson.wsdl|
|UDSPerson2|http://esb.services.wisc.edu/esbv2/UWDS/WebService/UDSPerson2.wsdl|https://integration.services.wisc.edu/wsdl/UDSPerson2.wsdl|
|ResourceBridge|http://esb.services.wisc.edu/esbv2/ResourceBridge/WebService/ResourceBridge.wsdl|_Coming soon_|
|ServiceAccount|http://esb.services.wisc.edu/esbv2/UWDS/WebService/ServiceAccount.wsdl|https://integration.services.wisc.edu/wsdl/ServiceAccount.wsdl|

## Migration Checklist

For service consumers or providers, the Integration Service uses the same usernames and passwords as the existing ESB.

### Service Consumers

- Confirm your application is using the new endpoints for SOAP or REST, listed above.
- The list of IP address ranges on the allow-list are documented [here](./user-guide.md) under Endpoints.
- If the IP addresses are not covered by the 3 CIDR blocks, then add your IP addresses to be on the allow-list, either by submitting a merge request to [edit this file](./terraform/post/variables.tf), or by contacting the API Team: api@doit.wisc.edu
- Confirm your application is no longer using the old ESB URLs: `esbtest.services.wisc.edu` or `esb.services.wisc.edu`
- If your application uses a certificate store or keystore and you receive an error similar to the following, you may need to import the Amazon Root CA certificate from (Distinguished Name: CN=Amazon Root CA 1,O=Amazon,C=US) from the [Amazon Trust Repository](https://www.amazontrust.com/repository/).
    >Exception: Untrusted Server Certificate Chain
- Review [differences in behavior](#behavior-differences) between the old ESB and the Integration Service. If you have any questions or concerns please contact the API Team at api@doit.wisc.edu.

### Service Providers

- For SOAP service providers, confirm your WSDLs are added to the [WSDL repository](https://git.doit.wisc.edu/interop/integration-service/wsdl).
- For REST service providers, contact us if you don't see your service listed in this guide: api@doit.wisc.edu
- Confirm your backend allows requests from the Integration Service by allowing the IP addresses. Test: `3.230.240.5` Prod: `3.19.12.147`
- Run your integration tests against the new endpoints for test and prod.

## Behavior Differences
Differences in behavior between the old ESB and Integration Service.

|ESB|Integration Service|
|---|-------------------|
|Accounts with access to a REST service could make requests to a resource and sub-resources as long as the requested URL started with the path the account is allowed to access. <br><br>For example: An account that has GET access for `my-test-service/api/v1/item` would be allowed to request `my-test-service/api/v1/item/123`. <br><br>It would also be able to request `my-test-service/api/v1/item-list/123` (assuming it exists) because the request url still starts with the allowed path.|Accounts with access to a REST service can make requests to a resource as long as the requested URL **equals** the path the account is allowed to access **OR** starts with the allowed path followed by a slash and then the requested sub-resources. <br><br>For example: An account that has GET access for `my-test-service/api/v1/item` would be allowed to request `my-test-service/api/v1/item/123` (same as ESB).<br><br>It would **NOT** be able to access `my-test-service/api/v1/item-list/123` because the request url does not start with the allowed path followed by a slash.<br><br>This change ensures that accounts can only request authorized paths.|
